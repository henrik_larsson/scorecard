var gulp = require('gulp');
var $ = require('gulp-load-plugins')();

var del = require('del');
var jadeConfig = require('./gulp-config/jade-config');
var jsConfig = require('./gulp-config/js-config');
var path = require('path');
var merge = require('merge-stream');
var runSequence = require('run-sequence');
var scssConfig = require('./gulp-config/scss-config');

var tasks = {
    clean: 'clean',
    default: 'default',
    distDev: 'dist:dev',
    distProd: 'dist:prod',
    distWatch: 'dist:watch',
    jade: 'jade',
    jadeClean: 'jade:clean',
    jsClient: 'js-client',
    jsCleanClient: 'js-client:clean',
    jsServer: 'js-server',
    jsCleanServer: 'js-server:clean',
    libs: 'libs',
    libsClean: 'libs:clean',
    scss: 'scss',
    scssClean: 'scss:clean',
    serve: 'serve'
};

// Main task
gulp.task(tasks.default, [tasks.serve]);

// Running the server
gulp.task(tasks.serve, function() {
    $.nodemon({
        cwd: 'dist',
        ext: 'js',
        script: 'server.js',
        watch: ['server/', 'server.js']
    })
});

// Distribution tasks
gulp.task(tasks.distDev, function(done) {
    runSequence(
        tasks.jade,
        tasks.jsClient,
        tasks.jsServer,
        tasks.libs,
        tasks.scss,
        done
    );
});

gulp.task(tasks.distWatch, [tasks.distDev], function() {
    gulp.watch(jadeConfig.sources, [tasks.jade]);
    gulp.watch(jsConfig.client.sources, [tasks.jsClient]);
    gulp.watch(jsConfig.server.sources, [tasks.jsServer]);
    gulp.watch(scssConfig.sources, [tasks.scss]);
});

gulp.task(tasks.clean, [tasks.jadeClean, tasks.jsCleanClient, tasks.jsCleanServer, tasks.libsClean, tasks.scssClean]);

// Js
gulp.task(tasks.jsClient, [tasks.jsCleanClient], function() {
    return gulp.src([jsConfig.client.sources, jsConfig.tsTypeDefinitions])
            .pipe($.typescript($.typescript.createProject('tsconfig.json')))
            .pipe(gulp.dest(jsConfig.client.destination));
});

gulp.task(tasks.jsCleanClient, function(done) {
    del(path.join(jsConfig.client.destination, '**')).then(function() { done(); });
});

gulp.task(tasks.jsServer, [tasks.jsCleanServer], function() {
    return merge(
        gulp.src([jsConfig.server.sources, jsConfig.tsTypeDefinitions])
            .pipe($.typescript())
            .pipe(gulp.dest(jsConfig.server.destination)),
        gulp.src([jsConfig.server.bootstrap, jsConfig.tsTypeDefinitions])
            .pipe($.typescript())
            .pipe(gulp.dest(jsConfig.server.bootstrapDestination)));
});

gulp.task(tasks.jsCleanServer, function(done) {
    del([
        path.join(jsConfig.server.destination, '**'),
        path.join(jsConfig.server.bootstrapDestination, 'server.js')])
        .then(function() { done(); });
});

gulp.task(tasks.libs, function() {
    return gulp.src(jsConfig.libs)
        .pipe(gulp.dest('dist/node_modules'));
});

gulp.task(tasks.libsClean, function() {
    del('dist/node_modules').then(function() { done(); });
});

// Scss
gulp.task(tasks.scss, [tasks.scssClean], function() {
    return gulp.src(scssConfig.sources)
        .pipe($.sass())
        .pipe(gulp.dest(scssConfig.destination));
});

gulp.task(tasks.scssClean, function(done) {
    del(path.join(scssConfig.destination, '**')).then(function() { done(); });
});

// Jade
gulp.task(tasks.jade, [tasks.jadeClean], function() {
    return gulp.src(jadeConfig.sources)
        .pipe($.jade())
        .pipe(gulp.dest(jadeConfig.destination));
});

gulp.task(tasks.jadeClean, function(done) {
    del(path.join(jadeConfig.destination, '**')).then(function() { done(); });
});
