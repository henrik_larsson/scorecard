module.exports = {
    client: {
        sources: 'client/js/**/*.ts',
        destination: 'dist/client/js/'
    },
    libs: [
        'node_modules/es6-shim/es6-shim.min.js',
        'node_modules/systemjs/dist/system-polyfills.js',
        'node_modules/angular2/es6/dev/src/testing/shims_for_IE.js',
        'node_modules/angular2/bundles/angular2-polyfills.js',
        'node_modules/systemjs/dist/system.src.js',
        'node_modules/rxjs/bundles/Rx.js',
        'node_modules/angular2/bundles/angular2.dev.js',
        'node_modules/angular2/bundles/router.dev.js'
    ],
    server: {
        sources: 'server/**/*.ts',
        destination: 'dist/server/',
        bootstrap: 'server.ts',
        bootstrapDestination: 'dist/'
    },
    tsTypeDefinitions: 'typings/main.d.ts'
};