var express = require('express');
var bodyParser = require('body-parser');
var path = require('path');

var courseRoutes = require('./server/routes/course');

var server = express();
server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: false }));

server.use('/libs', express.static('node_modules'));
server.use('/css', express.static('client/css'));
server.use('/js', express.static('client/js'));
server.use('/partials', express.static('client/templates'));
server.use('/images', express.static('client/images'));

server.get('/partials/:id', function(request, response) {
    response.render(request.params.id);
});

server.use('/api/courses', courseRoutes);

server.get('*', function(request, response) {
    response.sendFile(path.join(__dirname, '/client/templates/index.html'));
});

server.listen(8080);

console.log('Listening on port 8080...');