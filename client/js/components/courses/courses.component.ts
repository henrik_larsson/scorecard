import {Component} from 'angular2/core';

@Component({
    selector: 'courses',
    templateUrl: 'partials/components/courses/courses.html'
})

export class CoursesComponent {
    ui = {
        showAddCourse: false,
        cancelAddCourse: cancelAddCourse,
        showAddCourseForm: showAddCourseForm
    }
}

function cancelAddCourse() {
    this.ui.showAddCourse = false;
}

function showAddCourseForm() {
    this.ui.showAddCourse = true;
}

// ((angular) => {
//     angular.module('Scorecard').controller('CourseController', ['$scope', '$resource', courseController]);
//
//     function courseController($scope, $resource) {
//         var courseResource = $resource('/api/courses/:courseId', {courseId: '@Id'});
//         var ui = {
//             showAddCourse: false,
//             cancelAddCourse: cancelAddCourse,
//             showAddCourseForm: showAddCourseForm
//         };
//
//         $scope.courses = courseResource.query();
//         $scope.addCourse = addCourse;
//         $scope.ui = ui;
//         $scope.range = range;
//
//         function cancelAddCourse() {
//             ui.showAddCourse = false;
//         }
//
//         function showAddCourseForm() {
//             ui.showAddCourse = true;
//         }
//
//         function addCourse(newCourse) {
//             courseResource.save(newCourse, function(data) {
//                 console.log('save', data);
//                 $scope.courses = courseResource.query();
//             });
//         }
//
//         function range(lowerBound, upperBound) {
//             console.log('range: ', lowerBound, ', ', upperBound);
//             var range = [];
//             for (var i = lowerBound; i <= upperBound; i++) {
//                 range.push(i);
//             }
//             return range;
//         }
//     }
// })(angular);