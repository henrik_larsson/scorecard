import {Component} from 'angular2/core';
import {RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS} from 'angular2/router';
import {CoursesComponent} from '../courses/courses.component';
import {HeaderComponent} from '../header/header.component';
import {FooterComponent} from "../footer/footer.component";
import {PlayersComponent} from "../players/players.component";
import {OverviewComponent} from "../overview/overview.component";

@Component({
    selector: 'scorecard-app',
    templateUrl: 'partials/components/app/app.html',
    directives: [ROUTER_DIRECTIVES, HeaderComponent, FooterComponent],
    providers: [ROUTER_PROVIDERS]
})
@RouteConfig([
    { path: '/courses', name: 'Courses', component: CoursesComponent },
    { path: '/overview', name: 'Overview', component: OverviewComponent },
    { path: '/players', name: 'Players', component: PlayersComponent }
])

export class AppComponent {
    title = 'Scorecard'
}