var express = require('express');
var router = express.Router();

var courses = [
    {
        name: 'Teleborg',
        pars: [3, 3, 3, 3, 3, 3, 3, 3, 3]
    },
    {
        name: 'Kalmar',
        pars: [3, 3, 3, 3, 3, 3, 3, 3, 3,
            3, 3, 3, 3, 3, 3, 3, 3, 3]
    }
];

/* GET all courses */
router.get('/', function (request, response, next) {
    response.json(courses);
});

/* GET a course */
router.get('/:name', function (request, response, next) {
    var course = findCourse(request.params.name);

    if (!course) {
        response.status(404).send({error: 'Does not exist'});
    } else {
        response.json(course);
    }
});

/* POST a new course */
router.post('/', function (request, response, next) {
    var course = parseCourseRequest(request);
    if (findCourse(course.name)) {
        response.status(500).send({error: 'A course by that name already exists'});
    } else {
        console.log('adding new course ', course, request.body);
        addCourse(course);
        response.status(200).json(course.name);
    }
});


// Helpers
function findCourse(name) {
    return courses.filter(function (course) {
        return course.name === name
    })[0];
}

function addCourse(course) {
    courses.push(course);
}

function parseCourseRequest(courseRequest) {
    var pars = [];
    for (var i = 1; i <= courseRequest.body.nrOfHoles; i++) {
        pars.push(courseRequest.body['par' + i]);
    }
    return {
        name: courseRequest.body.name,
        pars: pars
    };
}

module.exports = router;